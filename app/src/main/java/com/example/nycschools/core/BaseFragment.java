package com.example.nycschools.core;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Rakesh Praneeth.
 * <p>
 * Base class for the Fragment to maintain the common properties that can be useful.
 */
abstract public class BaseFragment extends Fragment {

    /**
     * @return title provided by the class extending the base class
     */
    abstract @Nullable public String getTitle();

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getTitle());
    }

    /**
     * @param title - to set for the fragment.
     */
    private void setTitle(@Nullable String title) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity instanceof FragmentNavigation) {
            ((FragmentNavigation) activity).setActionBarTitle(title);
        }
    }

    /**
     * To navigate to the fragment
     *
     * @param fragment to navigate to.
     * @param tag      text to be tied to the fragment.
     */
    public void navigateToFragment(@NotNull Fragment fragment, @NotNull String tag) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity instanceof FragmentNavigation) {
            ((FragmentNavigation) activity).navigateToFragment(fragment, tag);
        }
    }
}
