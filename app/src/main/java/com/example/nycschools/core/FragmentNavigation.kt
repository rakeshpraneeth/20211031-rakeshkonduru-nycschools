package com.example.nycschools.core

import androidx.fragment.app.Fragment

/**
 * Created by Rakesh Praneeth.
 * To maintain methods that can be useful for fragment navigation and changing properties.
 *
 */
interface FragmentNavigation {

    /**
     * To navigate from one fragment to other.
     */
    fun navigateToFragment(fragment: Fragment, tag: String)

    /**
     * To set title to the toolbar.
     */
    fun setActionBarTitle(title: String?)
}