package com.example.nycschools.schoolslist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.databinding.ItemSchoolListBinding
import com.example.nycschools.models.School

/**
 * Created by Rakesh Praneeth.
 */
class SchoolPagedListAdapter(
    private val itemClick: (school: School) -> Unit
) : PagedListAdapter<School, SchoolPagedListAdapter.ItemViewHolder>(listDiffUtilCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemSchoolListBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_school_list, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, itemClick)
        }
    }

    class ItemViewHolder(private val binding: ItemSchoolListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: School, itemClick: (school: School) -> Unit) {
            with(binding) {
                item = obj
                executePendingBindings()
                itemLayout.setOnClickListener { itemClick.invoke(obj) }
            }
        }
    }

    companion object {
        var listDiffUtilCallback = object : DiffUtil.ItemCallback<School>() {
            override fun areItemsTheSame(oldItem: School, newItem: School) =
                oldItem.dbn == newItem.dbn

            override fun areContentsTheSame(oldItem: School, newItem: School) = oldItem == newItem

        }
    }
}