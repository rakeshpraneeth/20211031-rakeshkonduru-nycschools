package com.example.nycschools.schoolslist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.nycschools.R;
import com.example.nycschools.api.ApiConsumer;
import com.example.nycschools.core.BaseFragment;
import com.example.nycschools.databinding.FragmentSchoolsListBinding;
import com.example.nycschools.repository.SchoolsRepository;
import com.example.nycschools.repository.SchoolsRepositoryImpl;
import com.example.nycschools.schoolsatdetail.SchoolSatDetailsFragment;

import kotlin.Unit;

public class SchoolsListFragment extends BaseFragment {

    public static String TAG = "SchoolsListFragment";

    private SchoolsListViewModel viewModel;
    private FragmentSchoolsListBinding binding;
    private SchoolPagedListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSchoolsListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Can be replaced with dagger injection
        SchoolsRepository repository = new SchoolsRepositoryImpl(ApiConsumer.getInstance().getApiService());
        viewModel = new SchoolsListViewModel(repository);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        initializeRv();
        registerObservers();
    }

    private void initializeRv() {
        adapter = new SchoolPagedListAdapter(school -> {
            navigateToFragment(SchoolSatDetailsFragment.newInstance(school), SchoolSatDetailsFragment.TAG);
            return Unit.INSTANCE;
        });
        binding.schoolsList.setAdapter(adapter);
        binding.schoolsList.setHasFixedSize(true);
        binding.schoolsList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    /**
     * method to register for view model observers
     */
    public void registerObservers() {
        viewModel.getSchoolsList().observe(getViewLifecycleOwner(), schools -> {
            adapter.submitList(schools);
        });
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.list_of_schools_title);
    }
}