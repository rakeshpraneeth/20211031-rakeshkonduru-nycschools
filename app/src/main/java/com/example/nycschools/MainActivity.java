package com.example.nycschools;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.nycschools.core.FragmentNavigation;
import com.example.nycschools.schoolslist.SchoolsListFragment;

public class MainActivity extends AppCompatActivity implements FragmentNavigation {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragment(new SchoolsListFragment(), SchoolsListFragment.TAG, false);
    }

    /**
     * We can use JetPack Navigation component for navigation.
     *
     * @param fragment       to load.
     * @param tag            of the fragment.
     * @param addToBackStack to add or not to back stack.
     */
    private void loadFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    @Override
    public void navigateToFragment(@NonNull Fragment fragment, @NonNull String tag) {
        loadFragment(fragment, tag, true);
    }

    @Override
    public void setActionBarTitle(@Nullable String title) {
        String titleText = "";
        if (title != null && !title.isEmpty() && getSupportActionBar() != null) {
            titleText = title;
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(titleText);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home && getSupportFragmentManager().getBackStackEntryCount() > 0) {
            // If clicked back button on toolbar, go to previous screen.
            getSupportFragmentManager().popBackStack();
        }
        return super.onOptionsItemSelected(item);
    }
}
