package com.example.nycschools.repository

import com.example.nycschools.api.Resource
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolSatScoreDetails

/**
 * Created by Rakesh Praneeth.
 * Maintains the list of api calls that we can make for the schools feature.
 */
interface SchoolsRepository {

    suspend fun getListOfSchools(limit: Int, offset: Int): Resource<List<School>?>

    suspend fun getSchoolSatDetails(dbn: String): Resource<List<SchoolSatScoreDetails>?>
}