package com.example.nycschools.repository

import com.example.nycschools.api.ApiService
import com.example.nycschools.api.Resource
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolSatScoreDetails

/**
 * Created by Rakesh Praneeth.
 */
class SchoolsRepositoryImpl(private val apiService: ApiService) : SchoolsRepository {

    /**
     * Gets the list of schools
     */
    override suspend fun getListOfSchools(limit: Int, offset: Int): Resource<List<School>?> {
        val response = apiService.getListOfSchools(limit, offset)
        return if (response.isSuccessful) {
            Resource.success(response.body())
        } else {
            Resource.error(response.message())
        }
    }

    /**
     * Gets SAT details of the particular school.
     */
    override suspend fun getSchoolSatDetails(dbn: String): Resource<List<SchoolSatScoreDetails>?> {
        val response = apiService.getSchoolSatDetails(dbn)
        return if (response.isSuccessful) {
            Resource.success(response.body())
        } else {
            Resource.error(response.message())
        }
    }
}