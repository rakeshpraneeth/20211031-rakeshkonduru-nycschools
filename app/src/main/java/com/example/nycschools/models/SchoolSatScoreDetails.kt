package com.example.nycschools.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Rakesh Praneeth.
 */
@Parcelize
data class SchoolSatScoreDetails(
    @SerializedName("num_of_sat_test_takers") val numOfTestTakers: String?,
    @SerializedName("sat_critical_reading_avg_score") val readingAvgScore: String?,
    @SerializedName("sat_math_avg_score") val mathAvgScore: String?,
    @SerializedName("sat_writing_avg_score") val writingAvgScore: String?
) : Parcelable