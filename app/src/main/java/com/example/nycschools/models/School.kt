package com.example.nycschools.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Rakesh Praneeth.
 *
 * Data class to maintain data for the list of schools.
 */
@Parcelize
data class School(
    @SerializedName("school_name") val schoolName: String?,
    @SerializedName("overview_paragraph") val overview: String?,
    @SerializedName("academicopportunities1") val firstAcademicOpportunity: String?,
    @SerializedName("location") val location: String?,
    @SerializedName("phone_number") val phoneNumber: String?,
    @SerializedName("school_email") val email: String?,
    @SerializedName("dbn") val dbn: String
) : Parcelable