package com.example.nycschools.schoolsatdetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.nycschools.R;
import com.example.nycschools.api.ApiConsumer;
import com.example.nycschools.core.BaseFragment;
import com.example.nycschools.databinding.FragmentSchoolSatDetailsBinding;
import com.example.nycschools.models.School;
import com.example.nycschools.repository.SchoolsRepository;
import com.example.nycschools.repository.SchoolsRepositoryImpl;

import kotlinx.coroutines.Dispatchers;

/**
 * To show SAT details of the provided school.
 */
public class SchoolSatDetailsFragment extends BaseFragment {

    private static final String ARG_SCHOOL = "arg_school";
    public static final String TAG = "SchoolSatDetailsFragment";

    private FragmentSchoolSatDetailsBinding binding;

    private SchoolSatDetailsFragment() {
    }

    public static SchoolSatDetailsFragment newInstance(School school) {
        SchoolSatDetailsFragment fragment = new SchoolSatDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_SCHOOL, school);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSchoolSatDetailsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        School school = getArguments().getParcelable(ARG_SCHOOL);

        if (school == null) {
            binding.errorMessage.setVisibility(View.VISIBLE);
            return;
        }

        // Can use Dagger and ViewModelFactory
        SchoolsRepository repository = new SchoolsRepositoryImpl(ApiConsumer.getInstance().getApiService());
        SchoolSatDetailsViewModel viewModel = new SchoolSatDetailsViewModel(repository, Dispatchers.getIO());
        viewModel.getSatDetails(school);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setViewModel(viewModel);
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.detail_title);
    }
}