package com.example.nycschools.schoolsatdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.api.Status
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolSatScoreDetails
import com.example.nycschools.repository.SchoolsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View model to handle the logic for school sat details screen.
 * Created by Rakesh Praneeth.
 */
// Can use dagger and cover tests when have time.
class SchoolSatDetailsViewModel(
    private val repository: SchoolsRepository,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _noDataAvailable = MutableLiveData<Boolean>()
    val noDataAvailable: LiveData<Boolean> = _noDataAvailable

    private val _details = MutableLiveData<Pair<School, SchoolSatScoreDetails>>()
    val details: LiveData<Pair<School, SchoolSatScoreDetails>> = _details

    private val _status = MutableLiveData<Status>()
    val status: LiveData<Status> = _status

    fun getSatDetails(school: School) {
        _status.postValue(Status.PROCESSING)
        val exceptionHandler = CoroutineExceptionHandler { _, _ ->
            _status.postValue(Status.ERROR)
        }
        viewModelScope.launch(dispatcher + exceptionHandler) {
            val response = repository.getSchoolSatDetails(school.dbn)
            if (response.status == Status.SUCCESS) {
                val details = response.data
                if (details.isNullOrEmpty()) {
                    _noDataAvailable.postValue(true)
                } else {
                    // We can convert the response model to Domain objects that can provide data to UI.
                    val firstDetail = details[0]
                    _details.postValue(school to firstDetail)
                }
            }
            _status.postValue(response.status)
        }
    }
}