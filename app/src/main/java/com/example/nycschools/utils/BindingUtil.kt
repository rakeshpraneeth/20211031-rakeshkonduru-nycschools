package com.example.nycschools.utils

import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter

/**
 * Created by Rakesh Praneeth.
 */
object BindingUtil {

    @JvmStatic
    @BindingAdapter("showIfNotNullOrEmpty")
    fun View.showIfNotNullOrEmpty(input: String?) {
        setVisibility(input.isNullOrEmpty().not())
    }

    @JvmStatic
    @BindingAdapter("showIf")
    fun View.setVisibility(isVisible: Boolean?) {
        this.isVisible = isVisible ?: false
    }
}