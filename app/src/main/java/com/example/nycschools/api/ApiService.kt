package com.example.nycschools.api

import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolSatScoreDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Rakesh Praneeth.
 *
 * Contains the suspend functions to make the API calls.
 */
interface ApiService {

    /**
     * To get the list of schools for the provided offset and limit.
     * @param limit number of items needed from the api call.
     * @param offset position of the set from the total set to get the data.
     */
    @GET("resource/s3k6-pzi2.json")
    suspend fun getListOfSchools(
        @Query("\$limit") limit: Int,
        @Query("\$offset") offset: Int
    ): Response<List<School>?>


    /**
     * To get the school SAT details using the dbn number.
     * @param dbNumber db number of the school
     */
    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchoolSatDetails(
        @Query("dbn") dbNumber: String
    ): Response<List<SchoolSatScoreDetails>?>
}