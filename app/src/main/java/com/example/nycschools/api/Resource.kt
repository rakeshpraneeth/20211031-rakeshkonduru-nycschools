package com.example.nycschools.api

/**
 * Created by Rakesh Praneeth.
 *
 *  Gives the API result with status, data and any message if any error.
 */
class Resource<T> private constructor(val status: Status, val data: T?, val errorMessage : String? = null) {

    companion object {
        fun <T> success(data: T): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data
            )
        }

        fun <T> success(): Resource<T?> {
            return Resource(
                Status.SUCCESS,
                null
            )
        }

        fun <T> processing(): Resource<T?> {
            return Resource(
                Status.PROCESSING,
                null
            )
        }

        fun <T> error(message : String?): Resource<T?> {
            return Resource(
                Status.ERROR,
                null,
                message
            )
        }
    }
}