package com.example.nycschools.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Rakesh Praneeth.
 * To maintain the retrofit configurations.
 *
 */
class ApiConsumer {

    private var sRetrofit: Retrofit? = null

    private fun getRetrofit(): Retrofit {

        if (sRetrofit == null) {

            sRetrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return sRetrofit!!
    }

    /**
     * gives the API service that contains the list of api calls.
     */
    fun getApiService(): ApiService {
        return getRetrofit().create(ApiService::class.java)
    }

    companion object {
        private const val BASE_URL = "https://data.cityofnewyork.us/"

        private var sInstance: ApiConsumer? = null

        @JvmStatic
        fun getInstance(): ApiConsumer {
            if (sInstance == null) {
                sInstance = ApiConsumer()
            }
            return sInstance!!
        }
    }
}