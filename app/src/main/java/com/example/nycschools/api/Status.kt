package com.example.nycschools.api

/**
 * Created by Rakesh Praneeth.
 *
 * Different states of the API call.
 */
enum class Status {

    /**
     * Waiting for response.
     */
    PROCESSING,

    /**
     * Obtained response successfully.
     */
    SUCCESS,

    /**
     * Failed to get response.
     */
    ERROR
}