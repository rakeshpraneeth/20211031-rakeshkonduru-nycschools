package com.example.nycschools.repository

import com.example.nycschools.api.ApiService
import com.example.nycschools.api.Status
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolSatScoreDetails
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import retrofit2.Response

/**
 * Tests for [SchoolsRepositoryImpl]
 *
 * Created by Rakesh Praneeth.
 */
class SchoolsRepositoryImplTest {

    private val mockApiService = mockk<ApiService>()
    private lateinit var repository: SchoolsRepository

    @Before
    fun setUp() {
        repository = SchoolsRepositoryImpl(mockApiService)
    }

    @Test
    fun `should return Resource as success with data when list of schools api is successful`() {
        val mockList = listOf<School>(mockk(), mockk(), mockk())
        coEvery { mockApiService.getListOfSchools(any(), any()) } returns Response.success(mockList)
        runBlockingTest {
            val response = repository.getListOfSchools(10, 0)
            assertNotNull(response)
            assertEquals(Status.SUCCESS, response.status)
            assertEquals(3, response.data?.size)
        }
    }

    @Test
    fun `should return Resource as Error with message when list of schools api failed`() {
        coEvery {
            mockApiService.getListOfSchools(any(), any())
        } returns Response.error(
            400,
            ResponseBody.create(MediaType.parse("UTF-8"), "error")
        )
        runBlockingTest {
            val response = repository.getListOfSchools(10, 0)
            assertNotNull(response)
            assertEquals(Status.ERROR, response.status)
        }
    }

    @Test
    fun `should return Resource as success with data when detail api is successful`() {
        val mockList = listOf<SchoolSatScoreDetails>(mockk())
        coEvery { mockApiService.getSchoolSatDetails(any()) } returns Response.success(mockList)
        runBlockingTest {
            val response = repository.getSchoolSatDetails("20fdash")
            assertNotNull(response)
            assertEquals(Status.SUCCESS, response.status)
            assertEquals(1, response.data?.size)
        }
    }

    @Test
    fun `should return Resource as Error with message when detail api failed`() {
        coEvery {
            mockApiService.getSchoolSatDetails(any())
        } returns Response.error(
            400,
            ResponseBody.create(MediaType.parse("UTF-8"), "error")
        )
        runBlockingTest {
            val response = repository.getSchoolSatDetails("fhdasjk")
            assertNotNull(response)
            assertEquals(Status.ERROR, response.status)
        }
    }
}