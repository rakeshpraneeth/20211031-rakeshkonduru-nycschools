package com.example.nycschools.schoolsatdetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools.api.Resource
import com.example.nycschools.api.Status
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolSatScoreDetails
import com.example.nycschools.repository.SchoolsRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.IllegalStateException

/**
 * Created by Rakesh Praneeth.
 */
class SchoolSatDetailsViewModelTest {

    @get:Rule
    val instantExecutionRule = InstantTaskExecutorRule()

    private val dispatcher = TestCoroutineDispatcher()
    private val mockRepository = mockk<SchoolsRepository>()
    private val mockSchool = mockk<School>(relaxed = true) {
        coEvery { dbn } returns "fjdal"
    }
    private lateinit var viewModel: SchoolSatDetailsViewModel


    @Before
    fun setUp() {
        viewModel = SchoolSatDetailsViewModel(mockRepository, dispatcher)
    }

    @Test
    fun `should notify success and no data available if response is empty`() {
        runBlockingTest {
            coEvery { mockRepository.getSchoolSatDetails(any()) } returns Resource.success(listOf())
            viewModel.getSatDetails(mockSchool)
            viewModel.status.observeForever {
                assertEquals(Status.SUCCESS, it)
            }
            viewModel.noDataAvailable.observeForever {
                assertTrue(it)
            }
        }
    }

    @Test
    fun `should notify error and response failed to get`() {
        runBlockingTest {
            coEvery { mockRepository.getSchoolSatDetails(any()) } returns Resource.error("")
            viewModel.getSatDetails(mockSchool)
            viewModel.status.observeForever {
                assertEquals(Status.ERROR, it)
            }
        }
    }

    @Test
    fun `should notify details with status if able to get sat details`() {
        runBlockingTest {
            val mockDetail = mockk<SchoolSatScoreDetails>(relaxed = true)
            coEvery {
                mockRepository.getSchoolSatDetails(any())
            } returns Resource.success(listOf(mockDetail))
            viewModel.getSatDetails(mockSchool)
            viewModel.details.observeForever {
                assertEquals(mockSchool, it.first)
                assertEquals(mockDetail, it.second)
            }
            viewModel.status.observeForever {
                assertEquals(Status.SUCCESS, it)
            }
        }
    }

    @Test
    fun `should notify error if there is a coroutine exception`() {
        runBlockingTest {
            coEvery { mockRepository.getSchoolSatDetails(any()) } throws  IllegalStateException("")
            viewModel.getSatDetails(mockSchool)
            viewModel.status.observeForever {
                assertEquals(Status.ERROR, it)
            }
        }
    }

}